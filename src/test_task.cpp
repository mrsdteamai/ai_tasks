// #include <aikido/rviz/InteractiveMarkerViewer.hpp>
#include <Eigen/Dense>
#include <aikido/statespace/dart/MetaSkeletonStateSpace.hpp>
#include <aikido/constraint/TSR.hpp>
#include <aikido/constraint/NonColliding.hpp>
#include <dart/dart.hpp>
#include <iostream>
#include <aikido/rviz/InteractiveMarkerViewer.hpp>
#include <aikido/trajectory/Interpolated.hpp>
#include <aikido/util/CatkinResourceRetriever.hpp>
#include <dart/utils/urdf/DartLoader.hpp>
#include <dart/collision/CollisionDetector.hpp>
#include <dart/collision/CollisionOption.hpp>
#include <dart/collision/CollisionGroup.hpp>
#include <ai_environment/abb.hpp>
#include <ctime>

// --- ADDED BY JVEGA --- //
#include <aikido/statespace/Rn.hpp>
#include <sensor_msgs/JointState.h>
#include <ai_engine/JointTrajectory.h>
#include <ai_engine/planner.hpp>
#include <ai_tsr/tsr.h>
#include <ai_tsr/tsr_srv.h>
// ---------------------- //

using dart::dynamics::Frame;
using dart::dynamics::SimpleFrame;
using dart::collision::CollisionGroup;
using dart::collision::CollisionDetectorPtr;
using aikido::constraint::NonColliding;

// --- ADDED BY JVEGA --- //
using aikido::statespace::Rn;
using aikido::trajectory::InterpolatedPtr;

//#define TSRList int
#define TSRVec std::vector<aikido::constraint::TSR>
// ---------------------- //

static const std::string topicName("dart_markers");
static const double planningTimeout{5.};

// --- ADDED BY JVEGA --- //
class Counter
{
private:
    int currentID;
public:
    Counter() : currentID(0) {}
    int next() {return currentID++;}
};
// ---------------------- //

void makeBodyFromSTL(const dart::dynamics::SkeletonPtr& skeleton,
  const std::string& uri,
  const Eigen::Isometry3d& transform)
{
  using dart::dynamics::VisualAspect;
  using dart::dynamics::FreeJoint;
  using dart::dynamics::BodyNodePtr;
  using dart::dynamics::MeshShape;

  // Create skeleton
  BodyNodePtr bn = skeleton->createJointAndBodyNodePair<FreeJoint>(nullptr).second;

  // Resolves package:// URIs by emulating the behavior of 'catkin_find'.
  const auto resourceRetriever
    = std::make_shared<aikido::util::CatkinResourceRetriever>();

  // Load shape
  Eigen::Vector3d scale(1, 1, 1);
  std::shared_ptr<MeshShape> shape(new MeshShape(scale,
    MeshShape::loadMesh(uri, resourceRetriever)));
}

const dart::dynamics::SkeletonPtr makeBodyFromURDF(const std::string& uri,
  const Eigen::Isometry3d& transform)
{
  // Resolves package:// URIs by emulating the behavior of 'catkin_find'.
  const auto resourceRetriever
    = std::make_shared<aikido::util::CatkinResourceRetriever>();

  dart::utils::DartLoader urdfLoader;
  const dart::dynamics::SkeletonPtr skeleton = urdfLoader.parseSkeleton(
    uri, resourceRetriever);

  dynamic_cast<dart::dynamics::FreeJoint*>(skeleton->getJoint(0))->setTransform(transform);

  return skeleton;
}

// --- ADDED BY JVEGA --- //
ai_engine::JointTrajectory parseTrajectory(Rn stateSpace, InterpolatedPtr trajectory, std::vector<std::string>armJointNames)
{
  ai_engine::JointTrajectory armJointTrajectory;
  sensor_msgs::JointState jointState;
  Counter counter = Counter();

  jointState.header.stamp = ros::Time::now();

  int nofArmJoints = armJointNames.size();

  jointState.name.resize(nofArmJoints);
  jointState.position.resize(nofArmJoints);

  for (int i = 0; i < nofArmJoints; i++)
    jointState.name[i] = armJointNames[i];

  for (int i = 0; i < trajectory->getNumWaypoints(); i++)
  {
    auto state  = trajectory->getWaypoint(i);
    auto joints = stateSpace.getValue((const Rn::State*)state);

    jointState.header.seq = counter.next();

    for (int i = 0; i < nofArmJoints; i++)
      jointState.position[i] = joints[i];

    armJointTrajectory.waypoints.push_back(jointState);
  }

  return armJointTrajectory;
}
// ---------------------- //

TSRVec parseTSRs(std::vector<ai_tsr::tsr> tsrList)
{
  using aikido::constraint::TSR;

  TSRVec tsrVec;

  for (int i = 0; i < tsrList.size(); i++)
  {
    ai_tsr::tsr itsr = tsrList.at(i);
    TSR otsr;

    Eigen::MatrixXd R(3,3);
    Eigen::Isometry3d T;

    R = (Eigen::AngleAxisd(itsr.T_o_w.yaw,   Eigen::Vector3d::UnitZ())
      * Eigen::AngleAxisd(itsr.T_o_w.pitch, Eigen::Vector3d::UnitY())
      * Eigen::AngleAxisd(itsr.T_o_w.roll,  Eigen::Vector3d::UnitX())).toRotationMatrix();


    T(0,3) = itsr.T_o_w.x;
    T(1,3) = itsr.T_o_w.y;
    T(2,3) = itsr.T_o_w.z;

    T.linear() = R;

    otsr.mT0_w = T;

    R = (Eigen::AngleAxisd(itsr.T_w_e.yaw,   Eigen::Vector3d::UnitZ())
      * Eigen::AngleAxisd(itsr.T_w_e.pitch, Eigen::Vector3d::UnitY())
      * Eigen::AngleAxisd(itsr.T_w_e.roll,  Eigen::Vector3d::UnitX())).toRotationMatrix();

    T(0,3) = itsr.T_w_e.x;
    T(1,3) = itsr.T_w_e.y;
    T(2,3) = itsr.T_w_e.z;

    T.linear() = R;

    otsr.mTw_e = T;

    otsr.mBw(0,0) = itsr.B_w.x_b.lower;
    otsr.mBw(0,1) = itsr.B_w.x_b.upper;
    otsr.mBw(1,0) = itsr.B_w.y_b.lower;
    otsr.mBw(1,1) = itsr.B_w.y_b.upper;
    otsr.mBw(2,0) = itsr.B_w.z_b.lower;
    otsr.mBw(2,1) = itsr.B_w.z_b.upper;
    otsr.mBw(3,0) = itsr.B_w.roll_b.lower;
    otsr.mBw(3,1) = itsr.B_w.roll_b.upper;
    otsr.mBw(4,0) = itsr.B_w.pitch_b.lower;
    otsr.mBw(4,1) = itsr.B_w.pitch_b.upper;
    otsr.mBw(5,0) = itsr.B_w.yaw_b.lower;
    otsr.mBw(5,1) = itsr.B_w.yaw_b.upper;

    tsrVec.push_back(otsr);
  }

  return tsrVec;
}

int main(int argc, char** argv)
{
  using dart::dynamics::Skeleton;
  using dart::dynamics::SkeletonPtr;
  using aikido::statespace::dart::MetaSkeletonStateSpace;
  using aikido::constraint::TSR;
  using dart::dynamics::Frame;
  using dart::dynamics::SimpleFrame;

  // URDFs to load
  const std::string tableURDFUri("package://table/urdf/table.urdf");
  const std::string wallURDFUri("package://ai_environment/urdf/wall.urdf");
  const std::string binURDFURi("package://ai_environment/urdf/bin.urdf");
  int uuid;

  if (argc == 1) uuid = 0;
  else uuid = atoi(argv[1]);
  std::string objectURDFUri;

  switch(uuid)
  {
      case 0 : objectURDFUri = "package://ai_environment/urdf/sphere.urdf"; break;
      case 11 : objectURDFUri = "package://ai_environment/urdf/starkist.urdf"; break;
      case 12 : objectURDFUri = "package://ai_environment/urdf/foam.urdf"; break;
      case 13 : objectURDFUri = "package://ai_environment/urdf/sugar.urdf"; break;
      case 14 : objectURDFUri = "package://ai_environment/urdf/cheez_it.urdf"; break;
      case 15 : objectURDFUri = "package://ai_environment/urdf/mustard.urdf"; break;
      case 16 : objectURDFUri = "package://ai_environment/urdf/banana.urdf"; break;
      case 17 : objectURDFUri = "package://ai_environment/urdf/cleanser.urdf"; break;
      case 18 : objectURDFUri = "package://ai_environment/urdf/pitcher_base.urdf"; break;
  }

  Eigen::Matrix3d rot;

  // Poses for table and cage
  Eigen::Isometry3d tablePose(Eigen::Isometry3d::Identity());
  tablePose.translation() = Eigen::Vector3d(0, 0, 0);

  Eigen::Isometry3d binPose(Eigen::Isometry3d::Identity());
  binPose.translation() = Eigen::Vector3d(0.47, -0.0925, 0.97);
  rot = Eigen::AngleAxisd(3.14159, Eigen::Vector3d::UnitZ()) *
        Eigen::AngleAxisd(0, Eigen::Vector3d::UnitY()) *
        Eigen::AngleAxisd(1.5707, Eigen::Vector3d::UnitX());
  binPose.linear() = rot;
  Eigen::Isometry3d wallPose_right(Eigen::Isometry3d::Identity());
  wallPose_right.translation() = Eigen::Vector3d(0.4, -0.65, 2.0);
  rot = Eigen::AngleAxisd(M_PI_2, Eigen::Vector3d::UnitZ()) *
        Eigen::AngleAxisd(0, Eigen::Vector3d::UnitY()) *
        Eigen::AngleAxisd(0, Eigen::Vector3d::UnitX());
  wallPose_right.linear() = rot;

  Eigen::Isometry3d wallPose_left(Eigen::Isometry3d::Identity());
  wallPose_left.translation() = Eigen::Vector3d(-0.4, -0.65, 2.0);
  rot = Eigen::AngleAxisd(M_PI_2, Eigen::Vector3d::UnitZ()) *
        Eigen::AngleAxisd(0, Eigen::Vector3d::UnitY()) *
        Eigen::AngleAxisd(0, Eigen::Vector3d::UnitX());
  wallPose_left.linear() = rot;

  Eigen::Isometry3d wallPose_top(Eigen::Isometry3d::Identity());
  wallPose_top.translation() = Eigen::Vector3d(-0.5, -0.65, 1.8);
  rot = Eigen::AngleAxisd(0, Eigen::Vector3d::UnitZ()) *
        Eigen::AngleAxisd(0, Eigen::Vector3d::UnitY()) *
        Eigen::AngleAxisd(M_PI_2, Eigen::Vector3d::UnitX());
  wallPose_top.linear() = rot;

  Eigen::Isometry3d wallPose_4(Eigen::Isometry3d::Identity());
  wallPose_4.translation() = Eigen::Vector3d(-1.175, -1.2, 1.6);
  rot = Eigen::AngleAxisd(0, Eigen::Vector3d::UnitZ()) *
        Eigen::AngleAxisd(0, Eigen::Vector3d::UnitY()) *
        Eigen::AngleAxisd(M_PI_2, Eigen::Vector3d::UnitX());
  wallPose_4.linear() = rot;

  // Pose for arm
  Eigen::Isometry3d abbPose(Eigen::Isometry3d::Identity());
  abbPose.translation() = Eigen::Vector3d(0, 0.1989, 0.9);
  rot = Eigen::AngleAxisd(-M_PI_2, Eigen::Vector3d::UnitZ()) *
        Eigen::AngleAxisd(0, Eigen::Vector3d::UnitY()) *
        Eigen::AngleAxisd(0, Eigen::Vector3d::UnitX());
  abbPose.linear() = rot;

  // Pose for object
  Eigen::Isometry3d objectPose(Eigen::Isometry3d::Identity());
  if (argc <= 2){
      objectPose.translation() = Eigen::Vector3d(0, -.3, 1.0);
  }
  else if(argc == 8){
      objectPose.translation() = Eigen::Vector3d(atof(argv[2]), atof(argv[3]), atof(argv[4]));
      rot = Eigen::AngleAxisd(atof(argv[5]), Eigen::Vector3d::UnitX()) *
        Eigen::AngleAxisd(atof(argv[6]), Eigen::Vector3d::UnitY()) *
        Eigen::AngleAxisd(atof(argv[7]), Eigen::Vector3d::UnitZ());
      objectPose.linear() = rot;
    }
  else if(argc == 14){
    objectPose.translation() = Eigen::Vector3d(atof(argv[2]), atof(argv[3]), atof(argv[4]));
      rot = Eigen::AngleAxisd(atof(argv[5]), Eigen::Vector3d::UnitX()) *
        Eigen::AngleAxisd(atof(argv[6]), Eigen::Vector3d::UnitY()) *
        Eigen::AngleAxisd(atof(argv[7]), Eigen::Vector3d::UnitZ());
      objectPose.linear() = rot;

      wallPose_4.translation() = Eigen::Vector3d(atof(argv[8]), atof(argv[9]), atof(argv[10]));
      rot = Eigen::AngleAxisd(atof(argv[11]), Eigen::Vector3d::UnitZ()) *
        Eigen::AngleAxisd(atof(argv[12]), Eigen::Vector3d::UnitY()) *
        Eigen::AngleAxisd(atof(argv[13]), Eigen::Vector3d::UnitX());
      wallPose_4.linear() = rot;
  }
  else{
      throw std::runtime_error("Bad arguments. Usage: test_task uuid x y z r p y");
  }

  // ABB arm
  abb::Abb robot;
  SkeletonPtr robotSkeleton = robot.getSkeleton();
  dynamic_cast<dart::dynamics::FreeJoint*>(robotSkeleton->getJoint(0))->setTransform(abbPose);

  // Load table and object
  SkeletonPtr table = makeBodyFromURDF(tableURDFUri, tablePose);
  SkeletonPtr wall_right = makeBodyFromURDF(wallURDFUri, wallPose_right);
  SkeletonPtr wall_top = makeBodyFromURDF(wallURDFUri, wallPose_top);
  SkeletonPtr wall_left = makeBodyFromURDF(wallURDFUri, wallPose_left);
  SkeletonPtr wall_4 = makeBodyFromURDF(wallURDFUri, wallPose_4);
  SkeletonPtr bin = makeBodyFromURDF(binURDFURi, binPose);
  SkeletonPtr object = makeBodyFromURDF(objectURDFUri, objectPose);

  // Start the RViz viewer.
  ros::init(argc, argv, "abb");

  std::cout << "Starting viewer. Please subscribe to the '" << topicName
            << "' InteractiveMarker topic in RViz." << std::endl;
  aikido::rviz::InteractiveMarkerViewer* viewer = new aikido::rviz::InteractiveMarkerViewer(topicName);

  // Add everything to the viewer.
  viewer->addSkeleton(robotSkeleton);
  viewer->addSkeleton(table);
  viewer->addSkeleton(bin);
  viewer->addSkeleton(object);
  //viewer->addSkeleton(wall_right);
  //viewer->addSkeleton(wall_top);
  //viewer->addSkeleton(wall_left);
  //viewer->addSkeleton(wall_4);
  viewer->setAutoUpdate(true);

  // --- ADDED BY JVEGA --- //
  int nof_dimensions = 6;
  Eigen::VectorXd startConfiguration(nof_dimensions);

  ros::NodeHandle node;

  ros::Publisher arm_joint_pub = node.advertise<ai_engine::JointTrajectory>("/ABB140/trajectory", 10);
  ros::Publisher gripper_joint_pub = node.advertise<ai_engine::JointTrajectory>("/RobotiqC2/trajectory", 10);
  ros::ServiceClient tsrClient = node.serviceClient<ai_tsr::tsr_srv>("tsrs");
  // ---------------------- //

  // --- ADDED BY JVEGA --- //
  Eigen::VectorXd goalConfiguration(nof_dimensions);
  // ---------------------- //

  // auto untimedTrajectory = robot.planToConfiguration(
     // rightArmSpace, goalConfiguration, planningTimeout, nonCollidingConstraint);

  // ---------------------- //

  ai_tsr::tf_t tf;
  tf.x = objectPose.translation().x();
  tf.y = objectPose.translation().y();
  tf.z = objectPose.translation().z();
  auto euler = objectPose.linear().eulerAngles(0, 1, 2);
  tf.roll = euler(0);
  tf.pitch = euler(1);
  tf.yaw = euler(2);

  ai_tsr::tsr_srv srv;

  srv.request.uuid = uuid;
  srv.request.tf = tf;

  tsrClient.call(srv);

  std::vector<ai_tsr::tsr> tsrList;

  tsrList = srv.response.tsrs;
  TSRVec tsrVec = parseTSRs(tsrList);

  // ---------------------- //

  Eigen::Isometry3d Tw_e(Eigen::Isometry3d::Identity());
  Tw_e.translation() = Eigen::Vector3d(0, 0.22, 0.1);
  Eigen::Matrix3d rotation;
  rotation = Eigen::AngleAxisd(0, Eigen::Vector3d::UnitZ()) *
        Eigen::AngleAxisd(0, Eigen::Vector3d::UnitY()) *
        Eigen::AngleAxisd(-M_PI_2, Eigen::Vector3d::UnitX());
  Tw_e.linear() = rotation;

  TSR tsr;
  tsr.mT0_w = objectPose;
  tsr.mBw(5,0) = -M_PI;
  tsr.mBw(5,1) = M_PI;
  tsr.mTw_e = Tw_e;
  
  // Just to see tsr before planning.
  char c;
  std::cout << "Enter anything to start planning..." << std::endl;
  while((c = getchar()) != '\n' && c != EOF) {};

  std::clock_t start;
  double duration;

  start = std::clock();

   auto armSpace =
      std::make_shared<MetaSkeletonStateSpace>(robot.getArm());


  //startConfiguration << 1.00, -1.30, 0.00, 1.96, 0, 0.87;  // feasible but in collision

  //robot.setConfiguration(armSpace, startConfiguration);

  // Sets up collision constraints for planning.
  CollisionDetectorPtr collisionDetector = dart::collision::FCLCollisionDetector::create();

  auto nonCollidingConstraint =
      std::make_shared<NonColliding>(armSpace, collisionDetector);



  std::shared_ptr<CollisionGroup> armGroup = collisionDetector->createCollisionGroup();
  armGroup->addShapeFramesOf(armSpace->getMetaSkeleton().get());

  std::vector<dart::dynamics::BodyNodePtr> gripperNodes;
  for (int i = 0; i < gripperNodes.size(); i++)
  {
    armGroup->addShapeFramesOf(gripperNodes.at(i).get());
  }

  std::shared_ptr<CollisionGroup> envGroup = collisionDetector->createCollisionGroup();
  envGroup->addShapeFramesOf(table.get());
  envGroup->addShapeFramesOf(object.get());
  envGroup->addShapeFramesOf(bin.get());
  envGroup->addShapeFramesOf(wall_right.get());
  envGroup->addShapeFramesOf(wall_left.get());
  envGroup->addShapeFramesOf(wall_top.get());
  envGroup->addShapeFramesOf(wall_4.get());

  nonCollidingConstraint->addPairwiseCheck(armGroup, envGroup);

  // Eigen::VectorXd goalConfiguration(7);
  // // goalConfiguration << 5.65, -1.50, -0.26, 1.96, -1.15, 0.87, -1.43;  // relaxed home
  // goalConfiguration << 5.65, -1.00, 0.00, 1.96, 0, 0.87, -1.43;  // feasible but in collision

  auto gripper = robot.getEndEffector();
  viewer->addFrame(gripper.get());

  double timeout = 60.0;
  int trials = 200;

  std::vector<std::string> disableCollisions{
      "base_link"
      , "link_6"
      , "robotiq_85_adapter_link"
      , "robotiq_85_base_link"
      , "robotiq_85_left_finger_link"
      , "robotiq_85_right_finger_link"
      };
  //make planner object
  planner::Planner planner(robotSkeleton, disableCollisions, 0.0005f);
  // Dummy trajectory
  InterpolatedPtr untimedTrajectory;
    for (int i = 0; i < tsrVec.size(); i++)
  {
    robot.setConfiguration(armSpace,robot.getHomeConfiguration());
    viewer->visualizeTSR(tsrVec.at(i),10);
    untimedTrajectory = planner.planToTSR(
    armSpace, gripper, tsrVec.at(i), trials, timeout, nonCollidingConstraint);
    if (untimedTrajectory)
    {
      break;
    }
  }


  if (!untimedTrajectory) throw std::runtime_error("Failed to find a solution");

  aikido::rviz::InteractiveMarkerViewer* object_viewer = new aikido::rviz::InteractiveMarkerViewer(topicName);
  object_viewer->addSkeleton(object);
  object_viewer->setAutoUpdate(true);

  delete viewer;

  duration = ( std::clock() - start ) / (double) CLOCKS_PER_SEC;

  std::cout<<"printf: "<< duration <<'\n';
  ros::Duration(1.0).sleep();

  std::cout << "Press ENTER to move the arm..." << std::endl;
  while((c = getchar()) != '\n' && c != EOF) {};

  // --- ADDED BY JVEGA --- //
  int nofArmJoints = 6;
  std::vector<std::string> armJointNames;
  ai_engine::JointTrajectory armJointTrajectory;

  int nofGripperJoints = 6;
  std::vector<std::string> gripperJointNames;
  ai_engine::JointTrajectory gripperJointTrajectory;

  sensor_msgs::JointState jointState;

  armJointNames.resize(nofArmJoints);
  armJointNames[0] = "joint_1";
  armJointNames[1] = "joint_2";
  armJointNames[2] = "joint_3";
  armJointNames[3] = "joint_4";
  armJointNames[4] = "joint_5";
  armJointNames[5] = "joint_6";

  gripperJointNames.resize(nofGripperJoints);
  gripperJointNames[0] = "robotiq_85_left_knuckle_joint";
  gripperJointNames[1] = "robotiq_85_right_knuckle_joint";
  gripperJointNames[2] = "robotiq_85_left_inner_knuckle_joint";
  gripperJointNames[3] = "robotiq_85_right_inner_knuckle_joint";
  gripperJointNames[2] = "robotiq_85_left_finger_tip_joint";
  gripperJointNames[3] = "robotiq_85_right_finger_tip_joint";

  Rn stateSpace = Rn(nof_dimensions);

  armJointTrajectory = parseTrajectory(stateSpace, untimedTrajectory, armJointNames);

  std::cout << "Moving the arm..." << std::endl;
  arm_joint_pub.publish(armJointTrajectory);
  ros::Duration(4.0).sleep();
  // ---------------------- //

  std::cout << "Press ENTER to close the gripper..." << std::endl;
  while((c = getchar()) != '\n' && c != EOF) {};

  gripperJointTrajectory.waypoints.clear();
  Counter counter = Counter();

  jointState.header.stamp = ros::Time::now();

  jointState.name.resize(nofGripperJoints);
  jointState.position.resize(nofGripperJoints);

  for (int i = 0; i < nofGripperJoints; i++)
    jointState.name[i] = gripperJointNames[i];

  for (int i = 0; i < 4; i++)
  {
    jointState.position[i] = 0.8575f;
  }

  for (int i = 4; i < 6; i++)
  {
    jointState.position[i] = -0.8575f;
  }

  gripperJointTrajectory.waypoints.push_back(jointState);
  
  std::cout << "Closing the gripper..." << std::endl;
  gripper_joint_pub.publish(gripperJointTrajectory);
  ros::Duration(1.0).sleep();

  std::cout << "Press ENTER to reset robot..." << std::endl;
  while((c = getchar()) != '\n' && c != EOF) {};

  delete object_viewer;

  armJointTrajectory.waypoints.clear();

  jointState.header.stamp = ros::Time::now();

  jointState.name.resize(nofArmJoints);
  jointState.position.resize(nofArmJoints);

  for (int i = 0; i < nofArmJoints; i++)
    jointState.name[i] = armJointNames[i];

  for (int i = 0; i < nofArmJoints; i++)
  {
    jointState.position[i] = 0.0f;
  }

  armJointTrajectory.waypoints.push_back(jointState);
  
  std::cout << "Moving the arm back..." << std::endl;
  arm_joint_pub.publish(armJointTrajectory);
  ros::Duration(1.0).sleep();

  gripperJointTrajectory.waypoints.clear();

  jointState.header.stamp = ros::Time::now();

  jointState.name.resize(nofGripperJoints);
  jointState.position.resize(nofGripperJoints);

  for (int i = 0; i < nofGripperJoints; i++)
    jointState.name[i] = gripperJointNames[i];

  for (int i = 0; i < nofGripperJoints; i++)
  {
    jointState.position[i] = 0.0f;
  }

  gripperJointTrajectory.waypoints.push_back(jointState);
  
  std::cout << "Opening the gripper..." << std::endl;
  gripper_joint_pub.publish(gripperJointTrajectory);
  ros::Duration(1.0).sleep();

  std::cout << "Press <Ctrl> + C to exit." << std::endl;

  ros::spin();

  return 0;
}
